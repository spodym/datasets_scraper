# -*- coding: utf-8 -*-
import scrapy


class DataGovUkSpider(scrapy.Spider):
    """Spider for crawling datasets from data.gov.uk

    There is a bug in data.gov.uk search. No search page above 500 is working.
    This is an issue because it allows spider to crawl only 10k out of 46k
    datasets. By manually splitting crawls into "topics" we end up with only
    one topic being above 10k threshold: "Environment". Fortunatelly it has
    less than 20k datasets and by using trick with sorting by `sort=recent`
    and `sort=-recent` we can access all items.

    NOTE1: By scraping each dataset separately it would be possible to
    extract more metadata like license, responsible paty etc. I didn't know
    how could I used this data so I stayed with crawling search listing.

    NOTE2: Topic of dataset could be extracted from dataset page or search
    url. However since I am manually splitting crawl into topics (because of
    issue with >500page) I will use this data in request.meta field.
    """

    name = 'data_gov_uk'
    allowed_domains = ['data.gov.uk']

    DATA_GOV_UK_URL = 'https://data.gov.uk/{}'
    META_TOPIC_KEY = 'data_gov_topic'
    TOPICS = [
        'Business+and+economy',
        'Crime+and+justice',
        'Defence',
        'Education',
        'Environment',
        'Government',
        'Government+spending',
        'Health',
        'Mapping',
        'Society',
        'Towns+and+cities',
        'Transport'
    ]

    def start_requests(self):
        for topic in self.TOPICS:
            url = self.DATA_GOV_UK_URL.format('search?page=1&sort=recent&q=')
            url += f'&filters%5Btopic%5D={topic}'

            # NOTE: Passing dataset topic in meta dictionary.
            yield scrapy.Request(
                url=url,
                callback=self.parse,
                meta={
                    self.META_TOPIC_KEY: topic
                })

        # Yielding additional "reverse" crawl for "Environment" tab. It is too
        # big and  for unknown reason no page above 500 is working.
        # NOTE: sort=-recent

        url = self.DATA_GOV_UK_URL.format('search?page=1&sort=-recent&q=')
        url += '&filters%5Btopic%5D=Environment'
        yield scrapy.Request(
            url=url,
            callback=self.parse,
            meta={
                self.META_TOPIC_KEY: 'Environment'
            })


    def parse(self, response):
        for dataset in response.css('.dgu-results__result'):
            url = dataset.css('h2 a::attr(href)').extract_first()
            uuid = url.split('/')[2]

            yield {
                'uuid': uuid,
                'title': dataset.css('h2 a::text').extract_first(),
                'topic': response.meta[self.META_TOPIC_KEY],
                'url': self.DATA_GOV_UK_URL.format(url),
                'publisher': dataset.css('.published_by::text').extract_first(),
                'last_updated': dataset.css('.last_updated::text').extract_first(),
                'description': dataset.css('p::text')[-1].extract(),
            }

        next_page = response.css('a[rel="next"]::attr(href)').extract_first()
        if next_page is not None:
            # 1. Even if this is page 501 it is not a problem because crawler
            # won't be allowed on /search/site (redirect site)
            # 2. Topic must be passed in meta again.
            yield response.follow(
                next_page,
                callback=self.parse,
                meta={
                    self.META_TOPIC_KEY: response.meta[self.META_TOPIC_KEY]
                })
