Installing
----

```bash
pipenv install
```


Running
----

```bash
pipenv shell
scrapy crawl data_gov_uk -o ../datasets.json --logging INFO
```
