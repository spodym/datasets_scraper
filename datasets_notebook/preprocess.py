import csv
import json


def preprocess_datasets(datasets_path, csv_filepath):
    datasets = {}

    datasets_raw = {}
    with open(datasets_path) as datasets_fp:
        datasets_raw = json.load(datasets_fp)

    # Deduplicate
    for ds in datasets_raw:
        datasets[ds['uuid']] = ds

    # Save as csv
    with open(csv_filepath, 'w') as output_csv:
        fieldnames = ['title', 'topic', 'publisher']
        writer = csv.DictWriter(output_csv, fieldnames, extrasaction='ignore')

        writer.writeheader()

        for ds in datasets.values():
            ds['title'] = ds['title'].replace('\n', ' ')
            ds['topic'] = ds['topic'].replace('+', ' ')
            writer.writerow(ds)


if __name__ == '__main__':
    preprocess_datasets(
        datasets_path='../datasets.json',
        csv_filepath='datasets.csv')
